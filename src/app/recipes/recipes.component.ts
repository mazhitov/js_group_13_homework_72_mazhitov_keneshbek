import { Component, OnDestroy, OnInit } from '@angular/core';
import { Recipe } from '../shared/recipe.model';
import { RecipesService } from '../shared/recipes.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit, OnDestroy {
  recipes: Recipe[] = [];
  recipesChangeSubscription!:Subscription;
  recipesIsFetchingSubscription!:Subscription;
  recipesIsRemovingSubscription!:Subscription;
  isFetching = false;
  isRemoving = false;

  constructor(private recipesService: RecipesService) { }

  ngOnInit(): void {
    this.recipesChangeSubscription = this.recipesService.recipesChange.subscribe((recipes:Recipe[]) => {
      this.recipes = recipes;
    })

    this.recipesIsFetchingSubscription = this.recipesService.recipesIsFetching.subscribe((isFetching:boolean)=> {
      this.isFetching = isFetching;
    });
    this.recipesIsRemovingSubscription = this.recipesService.recipesIsRemoving.subscribe((isRemoving:boolean)=> {
      this.isRemoving = isRemoving;
    });
    this.recipesService.fetchRecipes();
  }

  ngOnDestroy(): void {
    this.recipesChangeSubscription.unsubscribe();
    this.recipesIsFetchingSubscription.unsubscribe();
    this.recipesIsRemovingSubscription.unsubscribe();
  }

  onRemove(id:string) {
    this.recipesService.removeRecipe(id).subscribe(() => {
      this.recipesService.fetchRecipes();
    });
  }
}
