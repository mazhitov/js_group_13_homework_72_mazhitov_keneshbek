import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { RecipesService } from '../shared/recipes.service';
import { Recipe } from '../shared/recipe.model';

@Component({
  selector: 'app-edit-add',
  templateUrl: './edit-add.component.html',
  styleUrls: ['./edit-add.component.css']
})
export class EditAddComponent implements OnInit, OnDestroy {
  recipeForm!: FormGroup;
  isEdit = false;
  editedId = '';
  isUploading = false;
  recipesUploadingSubscription!: Subscription;

  constructor(private recipesService: RecipesService,
              private router: Router,
              private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.recipesUploadingSubscription = this.recipesService.recipesIsUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });

    this.route.data.subscribe(data => {
      const recipe = <Recipe | null>data.recipe;

      if (recipe) {
        this.isEdit = true;
        this.editedId = recipe.id;
        this.setFormValue({
          name: recipe.name,
          imgUrl: recipe.imgUrl,
          description: recipe.description,
          ingredients: recipe.ingredients,
          steps: recipe.steps,
        });
      } else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          name: '',
          imgUrl: '',
          description: '',
          ingredients: '',
          steps: [],
        });
      }
    });

    this.recipeForm = new FormGroup({
      name: new FormControl('', Validators.required),
      imgUrl: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      ingredients: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      skills: new FormArray([]),
    });
  }

  setFormValue(value: { [key: string]: any }) {
    setTimeout(() => {
      const steps = value.steps;
      this.recipeForm.patchValue(value);
      const newFormArray = new FormArray([]);
      steps.forEach((step: {img: '', description: ''}) => {
        const  newFormGroup = new FormGroup({
          img: new FormControl(step.img, Validators.required ),
          description: new FormControl(step.description, Validators.required),
        })
        newFormArray.push(newFormGroup);
      });
      this.recipeForm.setControl('steps', newFormArray);
    });
  }


  Registrate() {
    const id = this.editedId || '';
    const newRecipe = new Recipe(
      id,
      this.recipeForm.get('name')?.value,
      this.recipeForm.get('imgUrl')?.value,
      this.recipeForm.get('description')?.value,
      this.recipeForm.get('ingredients')?.value,
      this.recipeForm.get('steps')?.value,
    );

    if (this.isEdit) {
      this.recipesService.editRecipe(newRecipe).subscribe(() => {
        this.recipesService.fetchRecipes();
        void this.router.navigate(['/']);
      });
    } else {
      this.recipesService.registrate(newRecipe)
        .subscribe(() => {
          void this.router.navigate(['/']);
        });
    }
  }

  fieldHasError(fieldName: string, errorType:string) {
    const field = this.recipeForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  groupFieldHasError(i:number, fieldName:string, errorType:string) {
    const fieldArray = <FormArray>this.recipeForm.get('steps');
    const fieldGroup = <FormGroup>fieldArray.controls[i];
    const field = fieldGroup.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  ngOnDestroy() {
    this.recipesUploadingSubscription.unsubscribe();
  }

  AddSteps() {
    const steps = <FormArray>this.recipeForm.get('steps');
    const stepsGroup = new FormGroup({
      img: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
    });
    steps.push(stepsGroup);
  }

  getControls() {
    const steps = <FormArray>this.recipeForm.get('steps');
    return steps.controls;
  }
}
