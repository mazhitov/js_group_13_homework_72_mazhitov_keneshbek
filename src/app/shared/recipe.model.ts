export class Recipe {
  constructor(
    public id: string,
    public name: string,
    public imgUrl: string,
    public description: string,
    public ingredients: string,
    public steps = [{img:'', description: ''}],
  ){}
}
