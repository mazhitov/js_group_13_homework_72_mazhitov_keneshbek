import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Recipe } from './recipe.model';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {
  recipesIsFetching = new Subject<boolean>();
  recipesIsUploading = new Subject<boolean>();
  recipesIsRemoving = new Subject<boolean>();
  recipesChange = new Subject<Recipe[]>();

  private recipes: Recipe[] = [];

  constructor(private http: HttpClient) {}

  fetchRecipes() {
    this.recipesIsFetching.next(true);
    this.http.get<{ [id: string]: Recipe }>('https://project-server-788da-default-rtdb.firebaseio.com/recipes.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const recipe = result[id];
          return new Recipe(id, recipe.name, recipe.imgUrl, recipe.description, recipe.ingredients, recipe.steps);
        });
      }))
      .subscribe(recipes => {
        this.recipes = recipes;
        this.recipesChange.next(this.recipes.slice());
        this.recipesIsFetching.next(false);
      }, () => {
        this.recipesIsFetching.next(false);
      });
  }

  fetchRecipe(id:string) {
    return this.http.get<Recipe | null>(`https://project-server-788da-default-rtdb.firebaseio.com/recipes/${id}.json`).pipe(
      map(result => {
        if (!result) {
          return null;
        }
        return new Recipe(id, result.name, result.imgUrl, result.description, result.ingredients, result.steps);
      }),
    );
  }

  registrate(recipe: Recipe) {
    const body ={
      name: recipe.name,
      imgUrl: recipe.imgUrl,
      description: recipe.description,
      ingredients: recipe.ingredients,
      steps: recipe.steps,
    };
    return this.http.post('https://project-server-788da-default-rtdb.firebaseio.com/recipes.json', body);
  }

  editRecipe(recipe: Recipe) {
    this.recipesIsUploading.next(true);
    const body ={
      name: recipe.name,
      imgUrl: recipe.imgUrl,
      description: recipe.description,
      ingredients: recipe.ingredients,
      steps: recipe.steps,
    };

    return this.http.put(`https://project-server-788da-default-rtdb.firebaseio.com/recipes/${recipe.id}.json`,
      body).pipe(
      tap(() => {
        this.recipesIsUploading.next(false);
      }, () => {
        this.recipesIsUploading.next(false);
      })
    );
  }

  removeRecipe(id:string) {
    this.recipesIsRemoving.next(true);
    return this.http.delete(`https://project-server-788da-default-rtdb.firebaseio.com/recipes/${id}.json`).pipe(
      tap(() => {
        this.recipesIsRemoving.next(false);
      }, () => {
        this.recipesIsRemoving.next(false);
      })
    );
  }
}
