import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { RecipesComponent } from './recipes/recipes.component';
import { NotFoundComponent } from './not-found.component';
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';
import { EditAddComponent } from './edit-add/edit-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    RecipesComponent,
    NotFoundComponent,
    RecipeDetailComponent,
    EditAddComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
