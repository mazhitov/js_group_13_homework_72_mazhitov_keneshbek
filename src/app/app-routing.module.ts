import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipesComponent } from './recipes/recipes.component';
import { NotFoundComponent } from './not-found.component';
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';
import { RecipeResolverService } from './shared/recipe-resolver.service';
import { EditAddComponent } from './edit-add/edit-add.component';

const routes: Routes = [
  {path: '',component: RecipesComponent},
  {path: 'recipes/:id',component: RecipeDetailComponent, resolve: {
    recipe: RecipeResolverService
    }},
  {path: 'recipes/:id/edit',component: EditAddComponent, resolve: {
      recipe: RecipeResolverService
    }},
  {path: 'add', component: EditAddComponent},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
